const getCube = 2 ** 3
console.log(`The cube of 2 is ${getCube}.`)


const address = ["San Isidro", "Cabuyao", "Laguna", "Philippines"]
const [brgy, municipality, province, country] = address
console.log(`I live at ${brgy}, ${municipality} ${province}, ${country}`)


const animal = {
	name: "Lolong",
	animalName: "Saltwater Crocodile",
	weight: 1075,
	size: "20 ft 3 in"
}
const {name, animalName, weight, size} = animal
console.log(`${name} is a ${animalName}. He weighed at ${weight} kgs with a measurement of ${size}.`)


const numbers = [1, 2, 3, 4, 5]

numbers.forEach((number) => console.log(number))


let reduceNumber = numbers.reduce((x, y) => x + y)
console.log(reduceNumber)

